

import java.util.Scanner;

import jm.edu.utech.ap.welcomelib.IWelcomeService;
import jm.edu.utech.ap.welcomeservice.WelcomeService;

public class Driver {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		IWelcomeService service = new WelcomeService();
		System.out.println("Please enter your name : ");
		String name = input.nextLine();
		System.out.println(
				service.getWelcomeMessage(name)
				);
		input.close();
		

	}

}
